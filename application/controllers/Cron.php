<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Cron Controller class.
 * 
 * @extends CI_Controller
 */
class Cron extends CI_Controller {
    
    private $contest_prefs;
    
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('dbsettings');
        $this->contest_prefs = $this->dbsettings->get_field('contest');
        $this->load->model('entry');
    }
    
    /**
     * twitter function.
     * 
     * @access public
     * @return void
     */
    public function twitter() {
		$params = $this->dbsettings->get_field('twitter');
		$this->load->library('twitter', (array) $params);
		
		$url = "https://api.twitter.com/1.1/search/tweets.json";
		$method = "GET";
		$search_terms = "#" . implode(" #", $this->contest_prefs->search_terms);
		$getfields = array('q' => $search_terms, 'result_type' => 'recent');
		
		$last_entry = $this->entry->get_last_entry('twitter');
		if ($last_entry) $getfields['since_id'] = $last_entry['remote_id'];
		
		$data = json_decode($this->twitter->setGetfields($getfields)->buildOauth($url, $method)->performRequest());
		$savedata = array();
		if (is_array($data->statuses)) {
			foreach ($data->statuses as $status) {
			    $image_url = isset($status->entities->media[0]) ? $status->entities->media[0]->media_url_https : false;
			    if ($image_url) {
	    		    $savedata[] = array(
	    		        'remote_id' => $status->id,
	    		        'type' => 'twitter',
	    		        'status' => 0,
	    		        'image_url' => $image_url,
	    		        'entry_text' => $status->text,
	    		        'created_time' => strtotime($status->created_at),
	    		        'username' => $status->user->screen_name,
	    		        'post_link' => "https://twitter.com/".$status->user->screen_name."/status/".$status->id,
	    		        'raw_data' => $status
	    		    );
			    }
			}
		}
		
		$count = $this->entry->save_new_entries($savedata, 'twitter');
		
		echo $count . " entries imported.";
    }
    
    /**
     * instagram function.
     * 
     * @access public
     * @return void
     */
    public function instagram() {
        $this->config->load('instagram');
		$params = $this->config->item('instagram');
		$this->load->library('instagram', $params);
        
        $this->instagram->setAccessToken($params['token']);
        $data = $this->instagram->getTagMedia($this->contest_prefs->search_terms[0]);
		
		$savedata = array();
		if (is_array($data->data)) {
			foreach ($data->data as $status) {
			    $has_tags = true;
			    $savedata = array();
			    foreach ($this->contest_prefs->search_terms as $tag) {
			    	if (!in_array($tag, $status->tags)) $has_tags = false;
			    }
			    if ($has_tags && $status->type == 'image') {
				    $savedata[] = array(
				        'remote_id' => $status->id,
				        'type' => 'instagram',
				        'status' => 0,
				        'image_url' => $status->images->standard_resolution,
				        'entry_text' => $status->caption->text,
				        'created_time' => strtotime($status->created_time),
				        'username' => $status->user->username,
				        'post_link' => $status->link,
				        'raw_data' => $status
				    );
			    }
			}
		}
        $count = $this->entry->save_new_entries($savedata, 'instagram');
        
        echo $count . " entries imported.";
    }
    
}