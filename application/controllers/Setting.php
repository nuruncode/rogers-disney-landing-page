<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Entries Admin Panel Controller class.
 * 
 * @extends CI_Controller
 */
class Setting extends CI_Controller {
    
    private $contest_prefs;
    
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        
        $this->load->model('dbsettings');
        $this->load->library("Aauth");
        
        if ( !$this->aauth->is_loggedin() ) redirect('/user');
    }
    
    public function index() {
        $error = false;
        if ($this->input->post('settings')) {
           foreach ($this->input->post('settings') as $slug => $setting) {
               $this->dbsettings->set_field($slug, $setting, $this->aauth->get_user_id());
           }
           $error = "All fields saved successfully.";
        }
        
        $data = array();
        $data['base_url'] = str_replace(':80', '', $this->config->item('base_url'));
        $data['settings'] = $this->dbsettings->get_all_fields();
        $data['error'] = $error;
        
        $this->load->library("Instagram", (array) $data['settings']['instagram']);
        $data['instagram_login'] = $this->instagram->getLoginUrl(array('basic','public_content'));
        
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        
        $this->load->view('components/header');
        $this->load->view('components/menu', array('type' => "Settings"));
        $this->load->view('admin/settings', $data);
        $this->load->view('components/footer');

    }
    
    public function instagram() {
        if ($this->input->get('code')) {
            $this->load->model('dbsettings');
            $settings = $this->dbsettings->get_field('instagram');
            $this->load->library("Instagram", (array) $settings);
            $token = $this->instagram->getOAuthToken($this->input->get('code'), true);
            $settings->token = $token;
            $this->dbsettings->set_field('instagram', $settings);
        }
        redirect("/setting");   
    }
    
}