<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {
    
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->helper('url');
    } 
    
    /**
     * index function.
     * 
     * @access public
     * @return void
     */
    public function index() {
        $error = false;
        if ($this->input->post('username') && $this->input->post('password')) {
            if ($this->aauth->login($this->input->post('username'), $this->input->post('password'), true)) {
                redirect('/entries');
            } else {
                $error = 'Bad username or password';
            }
        } else if ( $this->aauth->is_loggedin() ) {
            redirect('/entries');
        }
        
        $this->load->view('components/header');
        $this->load->view('admin/login', array('error' => $error));
        $this->load->view('components/footer', array('includescripts' => false));
    }
    
    /**
     * logout function.
     * 
     * @access public
     * @return void
     */
    public function logout() {
        $this->aauth->logout();
        redirect('/user');
    }
    
    /**
     * remind function.
     * 
     * @access public
     * @return void
     */
    public function remind() {
        $error = false;
        if ($this->input->post('email')) {
            if ($this->aauth->remind_password($this->input->post('email'))) {
                $error = "An email has been sent with a link to reset your password.";
            } else {
                $error = "We don't have that email on file.";
            }
        }
        
        $this->load->view('components/header');
        $this->load->view('admin/remind', array('error' => $error));
        $this->load->view('components/footer', array('includescripts' => false));
    }
    
    public function edit($id = NULL) {
        if ( !$this->aauth->is_loggedin() ) redirect('/user');
        
        $error = array();
        if ( $this->input->post('username') && $this->input->post('password') && $this->input->post('email') ) {
            $user_id = $this->input->post('id');
            $username = $this->input->post('username');
            $pass = $this->input->post('password');
            $match = $this->input->post('password') == $this->input->post('vpassword');
            $email = $this->input->post('email');
            if ( is_numeric($user_id) && $this->aauth->get_user($user_id) ) {
                if ($match) {
                    $this->aauth->update_user($user_id, $email, $pass, $username);
                    $error['type'] = 'success';
                    $error['text'] = "$username updated successfully.";
                } else {
                    $error['type'] = 'danger';
                    $error['text'] = "Passwords did not match. Please try again.";
                }
            } else if ($user_id == 'new') {
                if ($this->aauth->user_exist_by_email($email)) {
                    $error['type'] = 'warning';
                    $error['text'] = "User with email $email already exists.";
                } else if ($match) {
                    if ($this->aauth->create_user($email, $pass, $username)) {
                        $error['type'] = 'success';
                        $error['text'] = "$username created successfully.";
                    } else {
                        $error['type'] = 'danger';
                        $error['text'] = implode(', ', $this->session->flashdata('errors'));
                    }
                } else {
                    $error['type'] = 'danger';
                    $error['text'] = "Passwords did not match. Please try again.";
                }
            }
        }
        
        if (!count($error)) {
            $error = $this->session->flashdata();
        }
        
        $this->load->view('components/header');
        if ($id == NULL) {
            $this->load->view('components/menu', array('type' => "Manage Users"));
            $this->load->view('admin/manageusers', array('users' => $this->aauth->list_users(FALSE, FALSE, FALSE, TRUE), 'error' => $error));
        } else if ($id == 'new') {
            $this->load->view('components/menu', array('type' => "Create New User"));
            $this->load->view('admin/edituser', array('error' => $error, 'id' => $id));
        } else if ($id == 'me') {
            $id = $this->aauth->get_user_id();
            $this->load->view('components/menu', array('type' => "Edit My Account"));
            $this->load->view('admin/edituser', array('error' => $error, 'id' => $id, 'user' => $this->aauth->get_user($id)));
        } else {
            $this->load->view('components/menu', array('type' => "Edit User"));
            $this->load->view('admin/edituser', array('error' => $error, 'id' => $id, 'user' => $this->aauth->get_user($id)));
        }
        $this->load->view('components/footer');
    }
    
    public function delete($id = NULL) {
        if ( !$this->aauth->is_loggedin() ) redirect('/user');
        
        if ($id) {
            $this->aauth->ban_user($id);
            $this->session->set_flashdata(array('type'=>'danger', 'text'=>"User ID $id set to inactive."));
        }
        redirect("/user/edit");
    }
    
    public function undelete($id = NULL) {
        if ( !$this->aauth->is_loggedin() ) redirect('/user');
        
        if ($id) {
            $this->aauth->unban_user($id);
            $this->session->set_flashdata(array('type'=>'success', 'text'=>"User ID $id reactivated."));
        }
        redirect("/user/edit");
    }

}