<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Entries Admin Panel Controller class.
 * 
 * @extends CI_Controller
 */
class Entries extends CI_Controller {
    
    private $contest_prefs;
    
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->config->load('contest');
        $this->contest_prefs = $this->config->item('contest');
        
        $this->load->model('entry');
        $this->load->library("Aauth");
        
        if ( !$this->aauth->is_loggedin() ) redirect('/user');
    }
    
    /**
     * index function.
     * 
     * @access public
     * @return void
     */
    public function index() {
        $this->renderview($this->entry->get_new(), "New Entries");
    }
    
    /**
     * approved function.
     * 
     * @access public
     * @return void
     */
    public function approved() {
        $this->renderview($this->entry->get_approved(), "Approved Entries (Sortable)", true);
    }
    
    /**
     * rejected function.
     * 
     * @access public
     * @return void
     */
    public function rejected() {
        $this->renderview($this->entry->get_rejected(), "Rejected Entries");
    }
    
    /**
     * addnew function.
     * 
     * @access public
     * @return void
     */
    public function addnew() {
        // if posted data, store
        $error = false;
        if ($this->input->post('username')
            && $this->input->post('type')
            && $this->input->post('status')
            && $this->input->post('image_url')
            && $this->input->post('entry_text')
            ) {
            $entry = array(
                'remote_id' => 0,
    	        'type' => $this->input->post('type'),
    	        'status' => $this->input->post('status'),
    	        'image_url' => $this->input->post('image_url'),
    	        'entry_text' => $this->input->post('entry_text'),
    	        'created_time' => time(),
    	        'username' => $this->input->post('username'),
    	        'post_link' => 'manual entry',
    	        'raw_data' => 'manual entry'
            );
            
            $this->entry->save_manual($entry);
        }
        // show add new page
        $this->load->view('components/header');
        $this->load->view('components/menu', array('type' => "Add New Entry"));
        $this->load->view('admin/addnew', array('error' => $error));
        $this->load->view('components/footer');
    }
    
    /**
     * csvexport function.
     * 
     * @access public
     * @return void
     */
    public function csvexport() {
        $this->load->helper('download');
        $csvdata = $this->entry->get_all_csv();
        force_download($this->contest_prefs['contest_slug'] . "-" . date("Y-m-d") . ".csv", $csvdata);
    }
    
    /**
     * renderview function.
     * 
     * @access private
     * @param array $data
     * @param string $type
     * @return void
     */
    private function renderview($data, $type) {
        $this->load->view('components/header');
        $this->load->view('components/menu', array('type' => $type));
        $this->load->view('admin/entries', array('posts' => $data));
        $this->load->view('components/footer');
    }
}