<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Front (or public) class.
 * 
 * @extends CI_Controller
 */
class Front extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        $this->load->model('entry');
    }
    
    public function index() {
        $idiom = 'en';
        $this->load->view('public/main-'.$idiom, array("entries" => $this->entry->get_approved()));
    }
    
    public function fr() {
        $idiom = 'fr';
        $this->load->view('public/main-'.$idiom, array("entries" => $this->entry->get_approved()));
    }
    
}