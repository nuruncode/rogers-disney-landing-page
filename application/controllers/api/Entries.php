<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Entries API Controller class.
 * 
 * @extends CI_Controller
 */
class Entries extends CI_Controller {
    
    private $data;
    private $meta;
    
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('entry');
        $this->load->library("Aauth");
        
        $this->meta = array('code'=>200);
        $this->data = array();
    }
    
    /**
     * approve function.
     * 
     * @access public
     * @param mixed $id
     * @return void
     */
    public function approve($id) {
        if ( $this->aauth->is_loggedin() ) {
            $this->data['id'] = $id;
            $this->data['updated'] = $this->entry->set_approved($id) ? true : false;
            $this->data['status'] = $this->data['updated'] ? 'approved' : false;
            if (!$this->data['updated']) {
                $this->meta['code'] = 500;
                $this->meta['message'] = 'Something went wrong.';
            }
        } else {
            $this->meta['code'] = 400;
            $this->meta['message'] = 'You must be logged in to perform this action.';
        }
        $this->output();
    }
    
    /**
     * reject function.
     * 
     * @access public
     * @param mixed $id
     * @return void
     */
    public function reject($id) {
        if ( $this->aauth->is_loggedin() ) {
            $this->data['id'] = $id;
            $this->data['updated'] = $this->entry->set_rejected($id) ? true : false;
            $this->data['status'] = $this->data['updated'] ? 'rejected' : false;
            if (!$this->data['updated']) {
                $this->meta['code'] = 500;
                $this->meta['message'] = 'Something went wrong.';
            }
        } else {
            $this->meta['code'] = 400;
            $this->meta['message'] = 'You must be logged in to perform this action.';
        }
        $this->output();
    }
    
    /**
     * usersort function.
     * 
     * @access public
     * @return void
     */
    public function usersort() {
        if ( $this->aauth->is_loggedin() ) {
            $post = $this->input->post('sortlist');
            $entries = array();
            foreach ($post as $entry) {
                $entries[] = array('orderby' => $entry['orderby'], 'id' => $entry['id']);
            }
            $this->entry->update_entries($entries);
        } else {
            $this->meta['code'] = 400;
            $this->meta['message'] = 'You must be logged in to perform this action.';
        }
        $this->output();
    }
    
    /**
     * get function.
     * 
     * @access public
     * @param float $count (default: -1)
     * @param int $offset (default: 0)
     * @return void
     */
    // public function get($count = -1, $offset = 0) {
    //      $this->data['entries'] = $this->entry->get_approved($count, $offset);
    //      $this->output();
    // }
    
    /**
     * output function.
     * 
     * @access private
     * @return void
     */
    private function output() {
        http_response_code($this->meta['code']);
        echo json_encode(array(
            'meta' => $this->meta,
            'data' => $this->data
        ));
    }
    
}