<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['contest'] = array(
    'contest_table' => 'rogdis_entries',
    'settings_table' => 'rogdis_settings'
);