<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <!--pre><?php //print_r($users) ?></pre-->
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) : ?>
                    <tr<?php if ($user->banned) echo ' style="color:red"'?>>
                        <td><?php echo $user->id; ?></td>
                        <td><?php echo $user->name ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td>
                            <a href="/user/edit/<?php echo $user->id; ?>">Edit</a>
                            <?php if ($user->banned) : ?>
                            <a href="/user/undelete/<?php echo $user->id; ?>">Reactivate</a>
                            <?php else : ?>
                            <a href="/user/delete/<?php echo $user->id; ?>">Deactivate</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>