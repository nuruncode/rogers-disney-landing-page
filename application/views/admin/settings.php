<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php if (isset($error) && $error) : ?>
            <div class="alert alert-success" role="alert"><?php echo $error; ?></div>
            <?php endif; ?>
            <form method="post" action="/setting">
                <h3>General Contest Info</h3>
                <div class="form-group">
                    <label for="contest-name">Contest Name</label>
                    <input type="text" class="form-control" id="contest-name" name="settings[contest][name]" value="<?php echo $settings['contest']->name; ?>">
                </div>
                <div class="form-group">
                    <label for="contest-public_url">Public URL <small>(Automatic)</small></label>
                    <input type="text" class="form-control" id="contest-public_url" name="public_url" readonly value="<?php echo $base_url ?>">
                </div>
                <div class="form-group">
                    <label for="contest-search_terms">Hashtags / Search Terms <small>(a-z, 0-9, no special characters, case insensitive)</small></label>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="contest-search_terms" name="settings[contest][search_terms][]" value="<?php echo isset($settings['contest']->search_terms[0]) ? $settings['contest']->search_terms[0] : '' ?>">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="contest-search_terms1" name="settings[contest][search_terms][]" value="<?php echo isset($settings['contest']->search_terms[1]) ? $settings['contest']->search_terms[1] : '' ?>">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="contest-search_terms2" name="settings[contest][search_terms][]" value="<?php echo isset($settings['contest']->search_terms[2]) ? $settings['contest']->search_terms[2] : '' ?>">
                        </div>
                    </div>
                </div>
                <hr />
                
                <h3>Twitter</h3>
                <div class="form-group">
                    <label for="twitter-consumer_key">Consumer Key</label>
                    <input type="text" class="form-control" id="twitter-consumer_key" name="settings[twitter][consumer_key]" value="<?php echo $settings['twitter']->consumer_key ?>">
                </div>
                <div class="form-group">
                    <label for="twitter-consumer_secret">Consumer Secret</label>
                    <input type="text" class="form-control" id="twitter-consumer_secret" name="settings[twitter][consumer_secret]" value="<?php echo $settings['twitter']->consumer_secret ?>">
                </div>
                <div class="form-group">
                    <label for="twitter-oauth_access_token">OAuth Access Token</label>
                    <input type="text" class="form-control" id="twitter-oauth_access_token" name="settings[twitter][oauth_access_token]" value="<?php echo $settings['twitter']->oauth_access_token ?>">
                </div>
                <div class="form-group">
                    <label for="twitter-oauth_access_token_secret">OAuth Access Token Secret</label>
                    <input type="text" class="form-control" id="twitter-oauth_access_token_secret" name="settings[twitter][oauth_access_token_secret]" value="<?php echo $settings['twitter']->oauth_access_token_secret ?>">
                </div>
                <hr />
                
                <h3>Instagram</h3>
                <div class="form-group">
                    <div class="form-group">
                    <label for="instagram-apiKey">API Key</label>
                    <input type="text" class="form-control" id="instagram-apiKey" name="settings[instagram][apiKey]" value="<?php echo $settings['instagram']->apiKey ?>">
                </div>
                <div class="form-group">
                    <label for="instagram-apiSecret">API Secret</label>
                    <input type="text" class="form-control" id="instagram-apiSecret" name="settings[instagram][apiSecret]" value="<?php echo $settings['instagram']->apiSecret ?>">
                </div>
                <div class="form-group">
                    <label for="instagram-apiCallback">API Callback</label>
                    <input type="text" class="form-control" id="instagram-apiCallback" name="settings[instagram][apiCallback]" value="<?php echo $settings['instagram']->apiCallback ?>">
                </div>
                <div class="form-group">
                    <?php if (empty($settings['instagram']->token)) : ?>
                    <p>API Token <small>You are not connected to Instagram. <a href="<?php echo $instagram_login ?>">Connect to Instagram</a>.</small></p>
                    <?php else : ?>
                    <p>API Token <small>If you're not receiving entries from Instagram, <a href="<?php echo $instagram_login ?>">Reconnect to Instagram</a>.</small></p>
                    <?php endif; ?>
                    <input type="hidden" name="settings[instagram][token]" value="<?php echo $settings['instagram']->token ?>">
                </div>
                
                <button type="submit" class="btn btn-default">Save Settings</button>
            </form>
        </div>
    </div>
</div>