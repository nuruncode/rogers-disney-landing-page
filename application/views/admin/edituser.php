<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php if (isset($error) && count($error)) : ?>
            <div class="alert alert-<?php echo $error['type'] ?>" role="alert"><?php echo $error['text']; ?></div>
            <?php endif; ?>
            <form method="post" action="/user/edit">
                <?php if (isset($id)) : ?>
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <?php endif; ?>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo isset($user->name) ? $user->name : '' ?>">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group">
                    <label for="vpassword">Verify Password</label>
                    <input type="password" class="form-control" id="vpassword" name="vpassword">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?php echo isset($user->email) ? $user->email : '' ?>">
                </div>
                <button type="submit" class="btn btn-default">Save User</button>
            </form>
        </div>
    </div>
</div>