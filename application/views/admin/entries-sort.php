<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <ul id="sortable">
                <?php foreach ($posts as $post) : ?>
                <li class="ui-state-default theentry" data-toggle="tooltip" data-placement="bottom" title="<?php echo $post['entry_text']; ?>" data-id="<?php echo $post['id']; ?>">
                    <img src="<?php echo $post['image_url']; ?>">
                    <p class="smaller"><?php echo date('M d, h:m', $post['created_time']); ?> by <?php echo $post['username'] ?></p>
                    <button class="btn_reject btn btn-danger btn-xs" data-id="<?php echo $post['id'] ?>"><span class="glyphicon glyphicon-remove"></span></button>
                </li>
                <?php endforeach; ?>
            <ul id="sortable">
        </div>
    </div>
</div>