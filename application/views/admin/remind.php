<div class="container">
    <div class="row" style="padding-top: 30vh;">
        <div class="col-md-4 col-md-offset-4" style="border: 1px #aaa solid; padding: 20px; border-radius: 5px;">
            <h1 style="padding: 0 0 25px; margin: 0;">Contest Admin</h1>
            <?php if (isset($error) && $error) : ?>
            <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
            <?php endif; ?>
            <form method="post" action="/user/remind">
                <div class="form-group">
                    <label for="email" class="sr-only">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>