<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php if (isset($error) && $error) : ?>
            <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
            <?php endif; ?>
            <form method="post" action="/entries/addnew">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="facebook.com/[username]">
                </div>
                <div class="form-group">
                    <label for="entry_text">Post Text</label>
                    <textarea class="form-control" rows="3" id="entry_text" name="entry_text"></textarea>
                </div>
                <div class="form-group">
                    <label for="image_url">Shared Image URL</label>
                    <input type="text" class="form-control" id="image_url" name="image_url">
                </div>
                <hr />
                <p><strong>Social Network</strong></p>
                <div class="form-group">
                    <label class="radio-inline">
                        <input type="radio" name="type" id="sn1" value="facebook"> Facebook
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="type" id="sn2" value="twitter"> Twitter
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="type" id="sn3" value="instagram"> Instagram
                    </label>
                </div>
                <p><strong>Approval Status</strong></p>
                <div class="form-group">
                    <label class="radio-inline">
                        <input type="radio" name="status" id="status1" value="1"> Approved
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="status" id="status2" value="2"> Rejected
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="status" id="status3" value="0"> New
                    </label>
                </div>
                <button type="submit" class="btn btn-default">Add Entry</button>
            </form>
        </div>
    </div>
</div>