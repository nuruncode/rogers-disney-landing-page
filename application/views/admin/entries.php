<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($posts[0]) && $posts[0]['status'] == 1) : ?>
            <ul id="sortable">
            <?php else : ?>
            <ul id="unsortable">
            <?php endif; ?>
                <?php foreach ($posts as $post) : ?>
                <li class="ui-state-default theentry" data-toggle="tooltip" data-placement="bottom" title="<?php echo $post['entry_text']; ?>" data-id="<?php echo $post['id']; ?>">
                    <img src="<?php echo $post['image_url']; ?>">
                    <p class="smaller"><?php echo date('M d, h:m', $post['created_time']); ?> by <?php echo $post['username'] ?></p>
                    <?php if ($post['status'] != 2) : ?><button class="btn_reject btn btn-danger btn-xs" data-id="<?php echo $post['id'] ?>"><span class="glyphicon glyphicon-remove"></span></button><?php endif; ?>
                    <?php if ($post['status'] != 1) : ?><button class="btn_approve btn btn-success btn-xs" data-id="<?php echo $post['id'] ?>"><span class="glyphicon glyphicon-ok"></span></button><?php endif; ?>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>