<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta content="IE=edge" http-equiv="X-UA-Compatible">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<title>Rogers: Wireless, Internet, TV, Home Monitoring, and Home Phone</title>
		<meta name="description" content="With Wireless, Digital Cable TV, High Speed Internet, Home Phone and Smart Home Monitoring services we have everything you need for your home.">
		<meta name="google-site-verification" content="IDtvyPKshXW2xIAU0bcscnoBs9eCQp_tuAVfsl9WwKA">
		<link type="image/x-icon" href="http://www.rogers.com/cms/rogers/images/favicon.ico" rel="shortcut icon">
		<link rel="canonical" href="http://www.rogers.com/consumer/home">
		<meta name="dc.language" content="en" title="ISO639-2">
		<meta name="geo.region" content="on">
		<meta name="login.state" content="pre">
		<link rel="stylesheet" href="http://www.rogers.com/cms/common/css/bootstrap.min.css">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/cms/common/js/html5shiv.min.js"></script>
		<script src="/cms/common/js/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="http://www.rogers.com/cms/common/css/rui.css?v=1">
		<link rel="stylesheet" href="http://www.rogers.com/cms/common/css/rui-typeahead.css?v=1">
		<link rel="stylesheet" href="http://www.rogers.com/cms/common/css/rui-modal.css">
		<link rel="stylesheet" href="http://www.rogers.com/cms/common/css/jquery.owl-carousel.css">
		<link rel="stylesheet" href="http://www.rogers.com/cms/common/css/rui-icons/rui-icons.css?v=8">
		<link rel="stylesheet" href="http://www.rogers.com/cms/common/fonts/avenir-next.css">
		<style>.mboxDefault { visibility:hidden; }</style>
		<!-- END FORESEE NEW JS CONTENT --> <!-- Script for Bold Chat Co browse testing End -->
		<link type="text/css" href="http://www.rogers.com/cms/rogers/css/rogers.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/static/css/disney.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<script src="/static/js/parallax.min.js"></script>
		<script src="/static/js/jquery.countdown.min.js"></script>
		<style>
			[data-image-src="..//static/img/Disney-pic-newBG.png"]{
			width:100% !important;
			}
		</style>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-5986965-20', 'auto');
			ga('send', 'pageview');
			
		</script>
	</head>
	<body>
		<header xmlns:java-urldecode="java.net.URLDecoder" class="navbar navbar-fixed-top status-pre " role="banner" id="header">
			<div class="container">
				<div class="navbar-header">
					<button title="Menu" class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#main-menu">
					<span class="sr-only">Toggle navigation</span>
					<i class="rui-icon-menu"></i>
					</button>
					<a title="Rogers" class="navbar-brand" href="http://www.rogers.com/" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Rogers');">Rogers</a>
				</div>
				<div class="shadow"></div>
				<nav id="main-menu" class="collapse navbar-collapse bs-navbar-collapse">
					<ul id="nav-main" class="nav navbar-nav">
						<li class="visible-xs"><a href="http://www.rogers.com/web/content/store-locator">FIND A STORE</a></li>
						<li>
							<a index="1" id="main-nav-item-1" href="http://www.rogers.com/consumer/shop" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop');" >SHOP</a>
							<ul role="menu" class="dropdown-menu">
								<li>
									<a index="1" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop:Wireless');" id="main-nav-item-1-1" href="http://www.rogers.com/consumer/wireless">Wireless</a>
								</li>
								<li>
									<a index="2" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop:Bundles');" id="main-nav-item-1-2" href="http://www.rogers.com/consumer/bundles">Bundles</a>
								</li>
								<li>
									<a index="3" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop:TV');" id="main-nav-item-1-3" href="http://www.rogers.com/web/link/ptvBrowsePackagesFlowBegin?forwardTo=landing">TV</a>
								</li>
								<li>
									<a index="4" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop:Internet');" id="main-nav-item-1-4" href="http://www.rogers.com/consumer/internet">Internet</a>
								</li>
								<li>
									<a index="5" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop:Home Phone');" id="main-nav-item-1-5" href="http://www.rogers.com/consumer/home-phone">Home Phone</a>
								</li>
								<li>
									<a index="6" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop:Wireless Home Phone');" id="main-nav-item-1-6" href="http://www.rogers.com/consumer/wireless/wireless-home-phone">Wireless Home Phone</a>
								</li>
								<li>
									<a index="7" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Shop:Home Monitoring');" id="main-nav-item-1-7" href="http://www.rogers.com/consumer/home-monitoring">Home Monitoring</a>
								</li>
							</ul>
						</li>
						<li>
							<a index="2" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support');" id="main-nav-item-2" href="http://www.rogers.com/web/content/support">SUPPORT</a>
							<ul role="menu" class="dropdown-menu">
								<li>
									<a index="1" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support:Wireless');" id="main-nav-item-2-1" href="http://www.rogers.com/web/content/support?N=42+11">Wireless</a>
								</li>
								<li>
									<a index="2" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support:Internet');" id="main-nav-item-2-2" href="http://www.rogers.com/web/content/support?N=42+13">Internet</a>
								</li>
								<li>
									<a index="3" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support:TV');" id="main-nav-item-2-3" href="http://www.rogers.com/web/content/support?N=42+12">TV</a>
								</li>
								<li>
									<a index="4" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support:Home Phone');" id="main-nav-item-2-4" href="http://www.rogers.com/web/content/support?N=42+14">Home Phone</a>
								</li>
								<li>
									<a index="5" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support:Home Monitoring');" id="main-nav-item-2-5" href="http://www.rogers.com/web/content/support?N=42+283">Home Monitoring</a>
								</li>
								<li>
									<a index="6" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support:Billing and Accounts');" id="main-nav-item-2-6" href="http://www.rogers.com/web/content/support?N=42+134">Billing and Accounts</a>
								</li>
								<li>
									<a index="7" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Support:Additional Services');" id="main-nav-item-2-7" href="http://www.rogers.com/web/content/support?N=42+477">Additional Services</a>
								</li>
							</ul>
						</li>
						<li>
							<a index="3" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'MyRogers');" id="Myrogers" href="http://www.rogers.com/web/link/signin">MyROGERS</a>
						</li>
						<li>
							<a index="4" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Rewards');" id="main-nav-item-4" href="http://www.rogers.com/web/totes/#/rewards">REWARDS</a>
							<ul role="menu" class="dropdown-menu">
								<li>
									<a index="1" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Rewards:Browse');" id="main-nav-item-4-1" href="http://www.rogers.com/web/totes/#/rewards/browse">Browse</a>
								</li>
								<li>
									<a index="2" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Rewards:My WishList');" id="main-nav-item-4-2" href="http://www.rogers.com/web/totes/#/rewards/wishlist">My Wishlist</a>
								</li>
								<li>
									<a index="3" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Rewards:Membership');" id="main-nav-item-4-3" href="http://www.rogers.com/web/totes/#/rewards/membership">Membership</a>
								</li>
							</ul>
						</li>
						<li>
							<a index="5" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More');" id="main-nav-item-5" href="http://www.rogers.com/consumer/more">MORE</a>
							<ul role="menu" class="dropdown-menu">
								<li>
									<a index="1" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:Rogers NHL GameCentre LIVE');" id="main-nav-item-5-1" href="https://gamecentrelive.rogers.com/en/">Rogers NHL GameCentre LIVE</a>
								</li>
								<li>
									<a index="2" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:Rogers on Demand');" id="main-nav-item-5-2" href="http://www.rogersondemand.com/">Rogers on Demand</a>
								</li>
								<li>
									<a index="3" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:Shomi');" id="main-nav-item-5-3" href="https://shomi.rogers.com/">Shomi</a>
								</li>
								<li>
									<a index="4" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:Spotify');" id="main-nav-item-5-4" href="http://www.rogers.com/consumer/wireless/share-everything-plus#spotify">Spotify</a>
								</li>
								<li>
									<a index="5" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:Texture');" id="main-nav-item-5-5" href="https://www.texture.ca/en/">Texture</a>
								</li>
								<li>
									<a index="6" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:Connected');" id="main-nav-item-5-6" href="http://www.connectedrogers.ca/">Connected</a>
								</li>
								<li>
									<a index="7" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:TSC');" id="main-nav-item-5-7" href="https://www.theshoppingchannel.com/">TSC</a>
								</li>
								<li>
									<a index="8" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'More:Rogers Bank');" id="main-nav-item-5-8" href="https://www.rogersbank.com/en/">Rogers Bank</a>
								</li>
							</ul>
						</li>
						<li>
							<a index="6" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Promotions');" id="main-nav-item-6" href="http://www.rogers.com/consumer/promotions">PROMOTIONS</a>
							<ul role="menu" class="dropdown-menu">
								<li>
									<a index="1" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Promotions:Wireless');" id="main-nav-item-6-1" href="http://www.rogers.com/consumer/wireless/promotions">Wireless</a>
								</li>
								<li>
									<a index="2" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Promotions:Internet');" id="main-nav-item-6-2" href="http://www.rogers.com/consumer/internet/promotions">Internet</a>
								</li>
								<li>
									<a index="3" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Promotions:TV');" id="main-nav-item-6-3" href="http://www.rogers.com/consumer/tv/promotions">TV</a>
								</li>
								<li>
									<a index="4" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Promotions:Home Monitoring');" id="main-nav-item-6-4" href="http://www.rogers.com/consumer/home-monitoring/promotions">Home Monitoring</a>
								</li>
								<li>
									<a index="5" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Promotions:Home Phone');" id="main-nav-item-6-5" href="http://www.rogers.com/consumer/home-phone/promotions">Home Phone</a>
								</li>
								<li>
									<a index="6" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Promotions:Bundles');" id="main-nav-item-6-6" href="http://www.rogers.com/consumer/bundles/promotions">Bundles</a>
								</li>
							</ul>
						</li>
						<li>
							<a index="7" onclick="ga('send', 'event', 'Top Nav', '/disneyvacation', 'Business');" id="main-nav-item-7" href="http://www.rogers.com/business">BUSINESS</a>
						</li>
					</ul>
					<ul id="nav-context-bar" class="nav navbar-nav">
						<li id="username">
							<a href="http://www.rogers.com/web/link/signin">Sign In / Register <i class="rui-icon-arrow-right"></i>
							</a>
						</li>
						<li class="lang-toggle">
							<a data-short="FR" href="http://rogersdisney.publicisfb2.com/fr" onclick="ga('send', 'event', 'Top Nav', '/fr/disneyvacation', 'Language changed to FR');">Français</a>
						</li>
					</ul>
				</nav>
				<div id="nav-actions">
					<!-- <div id="search"><a title="Search" atm="search:global:header" href="#" data-toggle="modal" data-target="#modal-typeahead"><i class="rui-icon-search"></i><span class="sr-only">Search</span></a></div>-->
				</div>
			</div>
		</header>
		<div class="parallax-window carousel-inner" data-parallax="scroll" speed="0.1" data-image-src="/static/img/slide-bg.jpg">
			<div class="item active">
				<div class="slider-container col-md-7 col-sm-7 col-xs-12">
					<span class="headline" >
					You could win an unforgettable vacation to 1 of 5 <b>Disney Parks</b> around the world from Rogers.
					</span>
					<div class="hero-description" >
						<p>Use <b>Roam Like Home<sub>TM</sub></b> and share your family vacation photos for your chance to win a Disney Parks vacation.</p>
						<p>Every Disney Park blends regional charm with Disney characters and storytelling, legendary service, and the most innovative technology to give guests the chance to create unforgettable memories with family and friends.</p>
						<p>You and your family are eligible to enter no matter where the summer takes you, whether it&rsquo;s close to home, or around the world.</p>
						<p>Follow these steps for your chance to win. Enter by July 31<sup>st</sup>. </p>
					</div>
				</div>
				<div class="slider-col2 col-md-5 col-sm-5 col-xs-12" >
					<img src="/static/img/mmkd.png" alt=""/>
				</div>
			</div>
		</div>
		<div class="wrapper">
			<!--<section class="col-xs-12 timer-container">
				<h1 class=" col-md-12 text-center">Coming Soon!</h1>
				<div class="countdown-container">
				    <div id="getting-started"></div>
				    <div class="days-wrapper col-xs-3 text-center">
				        <span class="Days"></span> <br>Days
				    </div>
				    <div class="hours-wrapper col-xs-3 text-center">
				        <span class="Hours"></span> <br>Hours
				    </div>
				    <div class="minutes-wrapper col-xs-3 text-center">
				        <span class="Minutes"></span> <br>Minutes
				    </div>
				    <div class="seconds-wrapper col-xs-3 text-center">
				        <span class="Seconds"></span> <br>Seconds
				    </div>
				</div>
				</section>-->
			<section class="_3-col">
				<h2 class="col-md-12 text-center">Entering is easy!</h2>
				<article class="col-md-4 col-sm-4 col-xs-12 text-center">
					<img src="/static/img/video-icon.jpg" width="139" height="138" alt=""/>
					<div class="step">Step 1:</div>
					<div class="title">Capture a Moment</div>
					<div class="desc">Capture your favourite family moments while on vacation between July 1-31<sup style="vertical-align: text-bottom;">st</sup>.</div>
				</article>
				<article class="col-md-4 col-sm-4 col-xs-12 text-center">
					<img src="/static/img/hashtag-icon.jpg" width="138" height="138" alt=""/>
					<div class="step">Step 2:</div>
					<div class="title">Add Entry Hashtags</div>
					<div class="desc">In order for your family pic to qualify you’ll need to include the contest hashtags <b>#FromAnywhere</b> and <b>#RogersContest</b> in the post.</div>
				</article>
				<article class="col-md-4 col-sm-4 col-xs-12 text-center">
					<img src="/static/img/share-icon.jpg" width="137" height="137" alt=""/>
					<div class="step">Step 3:</div>
					<div class="title">Share</div>
					<div class="desc">Share your family moments on the Rogers Facebook page, Instagram or Twitter while traveling within Canada or abroad with <b>Roam Like Home<sub>TM</sub></b>.</div>
					<div class="col-xs-12">
						<div>
							<a href="https://www.facebook.com/Rogers" target="_blank" ><img class="social-share _fb" src="/static/img/facebook-share.jpg" width="44" height="44" alt=""/></a>
							<a href="https://www.instagram.com/rogers" target="_blank" ><img class="social-share _inst" src="/static/img/insta-share.jpg" width="44" height="44" alt=""/></a>
							<a href="https://twitter.com/rogers" target="_blank" ><img class="social-share _twtr" src="/static/img/twitter-share.jpg" width="44" height="44" alt=""/></a>
						</div>
					</div>
				</article>
			</section>
			<section class="_5-col">
				<h3 class=" col-md-12 text-center">You could win a vacation to 1 of 5 <b>Disney Parks</b> around the world.</h3>
				<article class="col-md-15 text-center">
					<div class="cuadro_intro_hover " style="background-color:#cccccc;">
						<p>
							<img src="/static/img/ShanghaiDisneyResort_thumbnail.png" class=" img-reponsive" alt="">
						</p>
						<div class="caption">
							<div class="blur"></div>
							<div class="caption-text">
								<h4 style="font-size:13px">Shanghai <em>Disney</em> Resort</h4>
								<!--<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
									<a class=" btn btn-default" href="http://trovacamporella.com"><span class="glyphicon glyphicon-plus"> INFO</span></a>-->
							</div>
						</div>
					</div>
				</article>
				<article class="col-md-15 text-center">
					<div class="cuadro_intro_hover " style="background-color:#cccccc;">
						<p>
							<img src="/static/img/DisneylandParis_thumbnail.png" class=" img-reponsive" alt="">
						</p>
						<div class="caption">
							<div class="blur"></div>
							<div class="caption-text">
								<h4 style="font-size:13px"><em>Disneyland</em> Paris</h4>
								<!--<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
									<a class=" btn btn-default" href="http://trovacamporella.com"><span class="glyphicon glyphicon-plus"> INFO</span></a>-->
							</div>
						</div>
					</div>
				</article>
				<article class="col-md-15 text-center">
					<div class="cuadro_intro_hover " style="background-color:#cccccc;">
						<p>
							<img src="/static/img/HongKongDisneyland_thumbnails.png" class=" img-reponsive" alt="">
						</p>
						<div class="caption">
							<div class="blur"></div>
							<div class="caption-text">
								<h4 style="font-size:13px">Hong Kong <em>Disneyland</em> Resort</h4>
								<!--<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
									<a class=" btn btn-default" href="http://trovacamporella.com"><span class="glyphicon glyphicon-plus"> INFO</span></a>-->
							</div>
						</div>
					</div>
				</article>
				<article class="col-md-15 text-center">
					<div class="cuadro_intro_hover " style="background-color:#cccccc;">
						<p>
							<img src="/static/img/WaltDisneyWorld---Florida_thumbnail.png" class=" img-reponsive" alt="">
						</p>
						<div class="caption">
							<div class="blur"></div>
							<div class="caption-text">
								<h4 style="font-size:13px"><em>Walt Disney World</em> Resort in Florida</h4>
								<!--<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
									<a class=" btn btn-default" href="http://trovacamporella.com"><span class="glyphicon glyphicon-plus"> INFO</span></a>-->
							</div>
						</div>
					</div>
				</article>
				<article class="col-md-15 text-center">
					<div class="cuadro_intro_hover " style="background-color:#cccccc;">
						<p>
							<img src="/static/img/Disneyland--California_thumbnail.png" class=" img-reponsive" alt="">
						</p>
						<div class="caption">
							<div class="blur"></div>
							<div class="caption-text">
								<h4 style="font-size:13px"><em>Disneyland</em> Resort in California</h4>
								<!--<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
									<a class=" btn btn-default" href="http://trovacamporella.com"><span class="glyphicon glyphicon-plus"> INFO</span></a>-->
							</div>
						</div>
					</div>
				</article>
             <p class="legal margin-top-large" style="text-align: right; padding-right: 15px;" >As to Disney artwork/properties: &copy;Disney</p>   
			</section>
			<section>
				<article class=" col-md-7 ">
					<div class="">
						<h1>You can enter whether you&rsquo;re at home or abroad.</h1>
                        <p class="margin-top-large">With <b>Roam Like Home<sub>TM</sub></b> on a <b>Share Everything<sub>TM</sub></b> plan, you have the only plan that travels with you in over 100 destinations around the world.</p>
                        <p>$5/day when roaming in the U.S. and $10/day when roaming in all other<br> eligible destinations.<img style="margin-left:40px;" src="/static/img/Roam_On_Button.png" width="89" height="24" alt=""/><sub style=" vertical-align:text-bottom;">TM</sub></p>
						<div class="btn_3_round text-center"><a href="http://www.rogers.com/consumer/wireless/travel?asc_refid=roamlikehome" onclick="ga('send', 'event', 'CTA', '/disneyvacation', '1 - Learn More');" >Learn more</a></div>
					</div>
					<div class="margin-top-xlarge">
						<p>Talk, text and share across Canada.</p>
						<p>No additional charge to use your device when roaming within Canada. Just make sure your data roaming is turned on in your device settings.</p>
					</div>
					<div class="btn_3_round text-center"><a href="http://www.rogers.com/consumer/wireless/network-coverage?asc_refid=coverage" onclick="ga('send', 'event', 'CTA', '/disneyvacation', '2 - Learn More');" >Learn more</a></div>
					<!--<div class="margin-top-xlarge">
						<p>See Who&rsquo;s Already Entered</p>
					</div>-->
                    <p class="legal margin-top-large" >U.S. service provided by AT&amp;T</p>
                    <br/>
                    <p class="legal-head"><a target="_blank" href="docs/2016_Disney_ROH_Final_Rules_and_Regs_June_29_EN.PDF">Rules + Regs.</a></p>
                    <p class="legal">Contest ends July 31, 2016. Open to Canadian residents of legal age. Tweet or share on Instagram or on the Rogers Facebook page a photo from where you travelled, are travelling or would like to travel with the hashtags <b>#FromAnywhere</b> and <b>#RogersContest</b>. Prize: One vacation prize for 4 to one of the following Disney Parks at the winner&rsquo;s choice:  The Walt <em>Disney World</em> Resort in Florida (approx. USD $11,927.92 ); the <em>Disneyland</em> Resort in California (approx. USD $10,720.00), <em>Disneyland</em> Paris (approx. USD $12,276.00); Shanghai <em>Disney</em> Resort (approx. USD  $11,304.00); or Hong Kong <em>Disneyland</em> Resort (approx. USD  $9,994.00). Vacation must be completed by September 30, 2016. Odds of winning depend on the number of entries. Correct answer to mathematical skill-testing question required.  No purchase necessary. &copy; 2016 Rogers Communications.</p>
				</article>
				<article class=" col-md-5 text-center">
					<img src="/static/img/phone-1.png" alt=""/>
				</article>
			</section>
		</div>
		<script language="javascript" type="text/javascript">
			$('#getting-started').countdown('2016/07/10').on('update.countdown', function(event) {
			     $(".Days").text(event.strftime("%D"));
			     $(".Hours").text(event.strftime("%H"));
			     $(".Minutes").text(event.strftime("%M"));
			     $(".Seconds").text(event.strftime("%S"));
			 });
		</script>
		
		<?php if (isset($entries)) : ?>
		<h2 class="col-md-12 text-center">See Who's Already Entered</h2>
		<div class="wrapper" id="entries">
	        <?php foreach($entries as $entry) : ?>
	        <article>
	            <div class="inner-entry">
    	            <div class="img-wrapper">
                        <img alt="Entry from <?php echo $entry['username']; ?>" src="/static/img/ph.png" data-src="<?php echo $entry['image_url'] ?>">
    	            </div>
    	            <div class="caption-text">
    	                <p><?php 
    	                $pos = strpos( $entry['entry_text'], ' ', 80);
    	                echo strlen($entry['entry_text']) > 80 && $pos ? substr( $entry['entry_text'], 0, $pos ) . '...' : $entry['entry_text'] 
    	                ?></p>
    	                <img src="/static/img/<?php echo $entry['type'] ?>.png" alt="<?php echo $entry['type'] ?>">
    	            </div>
	            </div>
	        </article>
	        <?php endforeach; ?>
		</div>
		<script language="javascript" type="text/javascript">
			$(function() {
    			$("#entries .img-wrapper img").unveil(20, function() {
                    $(this).load(function() {
                        this.style.opacity = 1;
                    });
                });
			});
		</script>
		<?php endif; ?>
		<div class="ls-cmp-wrap ls-1st" id="w1425243093018">
			<div class="iw_component" id="1425243093018">
				<section class="rui-help-login" style="width:100%;">
					<div class="container">
						<div class="row">
							<div class="col-sm-6 rui-help">
								<h2>Need Help? Learn how to:</h2>
								<div>
									<ul class="help-topics hidden-xs">
										<li>Manage your services</li>
										<li>Understand your bill</li>
										<li>Troubleshoot, and more.</li>
									</ul>
									<ul class="menu">
										<li>
											<a class="rui-cta-link" href="http://www.rogers.com/consumer/support/moving-your-services" onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Moving');" >Moving</a>
										</li>
										<li>
											<a class="rui-cta-link" href="http://www.rogers.com/consumer/support/contactus" onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Contact Us');" >Contact Us</a>
										</li>
										<li>
											<a class="rui-cta-link" href="http://www.rogers.com/web/content/store-locator" onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Find a store');" >Find a store</a>
										</li>
									</ul>
									<div class="button">
										<a class="rui-cta-link" href="http://www.rogers.com/web/content/support" onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Visit Support');" >Visit Support</a>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-sm-6 rui-login">
								<h2>MyRogers</h2>
								<form action="http://www.rogers.com/consumer/home?submit=true&amp;amp;componentID=1425243093018" method="post">
									<div class="left">
										<ul>
											<li>Manage your bill</li>
											<li>Check your usage</li>
											<li>Upgrade your device</li>
											<li>Learn about your perks</li>
										</ul>
									</div>
									<div class="right">
										<a class="btn rui-cta-badge" id="login-button" href="http://www.rogers.com/web/link/signin" title="Sign In" onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Sign In');" >Sign In</a>
										<a class="rui-cta-link" href="http://www.rogers.com/web/link/registration" title="New? Register now" onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'New? Register now');" >New? Register now</a>
									</div>
								</form>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<footer id="footer">
			<div class="container">
				<div class="row">
					<ul id="footer-col1" class="col-sm-3">
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Investor Relations');" href="http://www.rogers.com/web/ir/">Investor Relations</a>
						</li>
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Corporate Social Responsibility');" href="http://about.rogers.com/about/corporate-social-responsibility/">Corporate Social Responsibility</a>
						</li>
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Rogers Youth Fund');" href="http://www.rogersyouthfund.com/">Rogers Youth Fund</a>
						</li>
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'About Rogers');" href="http://about.rogers.com/about/">About Rogers</a>
						</li>
					</ul>
					<ul id="footer-col2" class="col-sm-3">
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Privacy and CRTC');" href="https://www.rogers.com/web/content/Privacy-CRTC">Privacy &amp; CRTC</a>
						</li>
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Careers');" href="http://jobs.rogers.com/?locale=en_US">Careers</a>
						</li>
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Community Forums');" href="http://communityforums.rogers.com/t5/community/categorypage/category-id/EnglishCommunity?profile.language=en">Community Forums</a>
						</li>
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Accessibility Services');" href="http://www.rogers.com/web/content/accessibility-services-wireless">Accessibility Services</a>
						</li>
					</ul>
					<ul id="footer-col3" class="col-sm-3">
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Terms and Conditions');" href="http://www.rogers.com/web/content/support-terms">Terms &amp; Conditions</a>
						</li>
						<li>
							<a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Site Map');" href="http://www.rogers.com/web/SITE_MAP.pg?customer_type=Residential">Site Map</a>
						</li>
					</ul>
					<ul id="footer-col4" class="col-sm-3">
						<li><a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Contact us');" href="http://www.rogers.com/consumer/support/contactus" title="Contact us"><i class="rui-icon-call"></i><span class="sr-only">Contact us</span></a></li>
						<li><a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Find a store');" href="http://www.rogers.com/web/content/store-locator" title="Find a store"><i class="rui-icon-locator"></i><span class="sr-only">Find a store</span></a></li>
					</ul>
					<div class="social-media">
						<span>Follow</span>
						<ul>
							<li><a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Twitter');" href="https://twitter.com/rogersbuzz/" title="Twitter"><i class="rui-icon-twitter"></i><span class="sr-only">Twitter</span></a></li>
							<li><a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Facebook');" href="https://www.facebook.com/Rogers/" title="Facebook"><i class="rui-icon-facebook"></i><span class="sr-only">Facebook</span></a></li>
							<li><a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Google Plus');" href="https://plus.google.com/+Rogers/posts" title="Google Plus"><i class="rui-icon-gplus"></i><span class="sr-only">Google Plus</span></a></li>
							<li><a onclick="ga('send', 'event', 'Bottom Nav', '/disneyvacation', 'Redboard');" href="http://redboard.rogers.com/" title="Redboard"><i class="rui-icon-redboard"></i><span class="sr-only">Redboard</span></a></li>
						</ul>
						<!-- Added for Qualtrics -->
						<div id="feedback-mobile-holder"></div>
						<!-- <a title="Provide feedback on this page" href="Javascript:void(0)" onclick="javascript:FSR.popFeedback()" class="btn">Feedback</a> -->
					</div>
				</div>
			</div>
		</footer>
		<script src="http://www.rogers.com/cms/common/js/bootstrap.min.js"></script>
		<script src="http://www.rogers.com/cms/common/js/jquery-migrate-1.2.1.min.js"></script>
		<script src="http://www.rogers.com/cms/common/js/rui.js"></script>
		<script src="http://www.rogers.com/cms/common/js/jquery.print.js"></script>
		<script src="http://www.rogers.com/cms/common/js/jquery.owl-carousel.min.js"></script> 
		<!--[if gt IE 9]><!-->  
		<script src="http://www.rogers.com/cms/common/js/rui-typeahead.js"></script> 
		<script src="http://www.rogers.com/cms/common/js/endeca-typeahead-global.js"></script> 
		<!--<![endif]--> 
		<script src="http://www.rogers.com/cms/js/SSOChat/SSOChat.js"></script> 
		<script src="/static/js/unveil.js"></script> 
		<script>
			$('.parallax-window').parallax({imageSrc: '/static/img/slide-bg.jpg'});
		</script>
	</body>
</html>