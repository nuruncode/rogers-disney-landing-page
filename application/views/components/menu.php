<nav class="navbar navbar-default">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Contest Admin - <span><?php echo $type ?></span></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" id="drop1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Entries <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="drop1">
                        <li><a href="/entries/addnew">Add New</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/entries">New</a></li>
                        <li><a href="/entries/approved">Approved</a></li>
                        <li><a href="/entries/rejected">Rejected</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/entries/csvexport">Export Entries</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" id="drop2" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="drop2">
                        <li><a href="/user/edit/new">Add New User</a></li>
                        <li><a href="/user/edit">Edit Users</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/user/edit/me">Change Password</a></li>
                    </ul>
                </li>
                <li><a href="/setting">Contest Settings</a></li>
                <li><a href="/user/logout">Logout</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>