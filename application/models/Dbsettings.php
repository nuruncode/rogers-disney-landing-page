<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Entry DB Model class.
 * 
 * @extends CI_Model
 */
class Dbsettings extends CI_Model {
    private $table;
    
    /**
     * __construct function.
     * 
     * @access public
     * @return Entry
     */
    public function __construct() {
        // Call the CI_Model constructor
        $this->load->database();
        parent::__construct();
        
        $this->config->load('contest');
        $this->contest_prefs = $this->config->item('contest');
        $this->table = $this->contest_prefs['settings_table'];
    }
    
    /**
     * get_field function.
     * 
     * @access public
     * @param string $slug
     * @return array
     */
    public function get_field($slug) {
        $settings = $this->db->where('slug', $slug)->get($this->table)->result_array();
        return json_decode($settings[0]['value']);
    }
    
    /**
     * set_field function.
     * 
     * @access public
     * @param string $slug
     * @param mixed $value
     * @return int/bool
     */
    public function set_field($slug, $value, $user_id = 0) {
        if (is_object($value) || is_array($value)) $value = json_encode($value);
        if ($this->get_field($slug)) $this->db->where('slug', $slug)->set('value', $value)->update($this->table);
        else $this->db->set(array('slug' => $slug, 'value' => $value, 'user_id' => $user_id))->insert($this->table);
        return $this->db->affected_rows();
    }
    
    /**
     * get_all_fields function.
     * 
     * @access public
     * @return array
     */
    public function get_all_fields() {
        $settings = $this->db->select('*')->get($this->table)->result_array();
        $output = array();
        foreach ($settings as $setting) {
            $output[$setting['slug']] = json_decode($setting['value']);
        }
        return $output;
    }
    
    
}