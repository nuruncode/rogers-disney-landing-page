<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Entry DB Model class.
 * 
 * @extends CI_Model
 */
class Entry extends CI_Model {
    private $table;
    
    /**
     * __construct function.
     * 
     * @access public
     * @return Entry
     */
    public function __construct() {
        // Call the CI_Model constructor
        $this->load->database();
        parent::__construct();
        
        $this->config->load('contest');
        $this->contest_prefs = $this->config->item('contest');
        $this->table = $this->contest_prefs['contest_table'];
    }
    
    /**
     * get_by_status function.
     * 
     * @access private
     * @param int $status
     * @return array
     */
    private function get_by_status($status) {
        return $this->db->select('id, type, status, image_url, entry_text, created_time, username, post_link')->where('status', intval($status))->get($this->table)->result_array();
    }
    
    /**
     * set_status function.
     * 
     * @access private
     * @param int $id
     * @param int $status
     * @return int/bool
     */
    private function set_status($id, $status) {
        $this->db->where('id', $id)->set('status', $status)->update($this->table);
        return $this->db->affected_rows();
    }
    
    /**
     * get_rejected function.
     * 
     * @access public
     * @return array
     */
    public function get_rejected() {
        return $this->get_by_status(2);
    }
    
    /**
     * get_approved function.
     * 
     * @access public
     * @param float $count (default: -1)
     * @param int $offset (default: 0)
     * @return array
     */
    public function get_approved($count = -1, $offset = 0) {
        if ($count > 0) $this->db->limit($count);
        if ($offset > 0) $this->db->offset($offset);
        return $this->db->select('id, type, status, image_url, entry_text, created_time, username, post_link')
                ->where('status', 1)
                ->order_by('orderby', 'ASC')
                ->order_by('created_time', 'DESC')
                ->get($this->table)
                ->result_array();
    }
    
    /**
     * get_new function.
     * 
     * @access public
     * @return array
     */
    public function get_new() {
        return $this->get_by_status(0);
    }
    
    /**
     * set_approved function.
     * 
     * @access public
     * @param int $id
     * @return array
     */
    public function get_all_csv() {
        $this->load->dbutil();
        $result = $this->db->select('id, type, status, image_url, entry_text, created_time, username, post_link')->get($this->table);
        return $this->dbutil->csv_from_result($result);
    }
    
    /**
     * set_rejected function.
     * 
     * @access public
     * @param int $id
     * @return int/bool
     */
    public function set_rejected($id) {
        return $this->set_status($id, 2);
    }
    
    /**
     * set_approved function.
     * 
     * @access public
     * @param int $id
     * @return int/bool
     */
    public function set_approved($id) {
        return $this->set_status($id, 1);
    }
    
    /**
     * save_new_entries function.
     * 
     * @access public
     * @param array $data
     * @return int (number of affected rows on success)/bool(FALSE on fail)
     */
    public function save_new_entries($data, $service) {
        $remote_ids = $this->get_compare_entries($service);
        foreach ($data as &$entry) {
            if (in_array($entry['remote_id'], $remote_ids)) {
                unset($entry);
                continue;
            }
            $entry['raw_data'] = (is_object($entry['raw_data'])) ? json_encode($entry['raw_data']) : $entry['raw_data'];
        }
        if (count($data)) return $this->db->insert_batch($this->table, $data);
        return 0;
    }
    
    public function save_manual($data) {
        return $this->db->insert($this->table, $data);
    }
    
    /**
     * update_entries function.
     * 
     * @access public
     * @param array $data
     * @return int (number of affected rows on success)/bool(FALSE on fail)
     */
    public function update_entries($data) {
        return $this->db->update_batch($this->table, $data, 'id');
    }
    
    /**
     * get_compare_entries function.
     * 
     * @access public
     * @param string $service
     * @return array
     */
    public function get_compare_entries($service) {
        $remote_ids = array();
        foreach ($this->db->select('remote_id')->where('type', $service)->get($this->table)->result_array() as $entry) {
            $remote_ids[] = $entry['remote_id'];
        }
        return $remote_ids;
    }
    
    /**
     * get_last_entry function.
     * 
     * @access public
     * @param string $service
     * @return array
     */
    public function get_last_entry($service) {
        return $this->db->where('type', $service)->order_by('created_time', 'DESC')->limit(1)->get($this->table)->result_array()[0];
    }
}