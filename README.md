#Rogers Disney Landing Page
Project docket number:	ROG169026

##Overview
The project calls for the creation of a contest landing page for a Rogers/Disney contest. The page will include a feed of social media entries from Twitter and Instagram that tag their posts with the contest hashtags and have been approved by an administrator.
In order to manage this feed we will need a backend system that an administrator can use to view and approve posts that contain the contest hashtag prior to them being populated on the site.

##Contest details
The contest will have users enter by tagging either a twitter or Instagram post with two hashtags.
Winners will be randomly chosen from the entries created during this time.
Features Required:
* User login
* Ability to preview posts with the contest hashtags
* Ability to approve posts to be populated on the landing page
* Ability to reorder posts to be populated on site
* Set up of hosting environment for site and backend.
* Site content will be both English and French
* Backend does not need to support a French interface

##Setup
+ Copy the folder structure to the web root of the server/domain you wish to host it on.
+ Open /application/config/database.php and fill in the fields to connect to the database.
+ Import contest.sql into your database.
+ Open a web browser and go to [yourserver.com]/user/ and login with the default user (username: contest, password: c0nt3$ts)
+ Change your password under Users->My Account, or remove the default account and add a new one under Users->Manage Users.
+ Go to Settings and fill out all the fields.
+ Setup a cron on your server to hit [yourserver.com]/cron/[social] every hour to retrieve new entries.

Your mileage may vary, however, there should be no more configuration necessary. For more information on setting up CodeIgniter, refer to [CI's documentation](http://www.codeigniter.com/user_guide/installation/index.html).

##Technical & Structural Overview
###Languages & Frameworks Used
####Server Side
* PHP
* CodeIgniter 3 Framework
* MySQL

####Client Side
* Bootstrap 3
* jQuery 2
* LESS (use builder of your choice)

###Controllers
####/api/Entries.php - This handles JSON access to the app.
+ approve - _requires authentication_ Approves a single entry with provided id (sets `status` to 1). *URL:* /api/entries/approve/[id] 
+ reject - _requires authentication_ Rejects a single entry with provided id (sets `status` to 2). *URL:* /api/entries/reject/[id]
+ usersort - _requires authentication_ Sets the order for all approved entries. Recieves post array of objects with structure {orderby: (int), id: (int)}. *URL:* /api/entries/usersort
+ get - _open but commented out_ Outputs a list of approved entries. Allows for count and offset in the URL, otherwise, outputs all entries. *URL:* /api/entries/get/[count int]/[offset int]
 
####/Cron.php - Handles getting new entries and storing in the Database. Meant to be handled by server cron.
+ twitter - _open_ Gets new twitter entries and stores in the DB. Filters out entries with no media, and only gets the latest entries since the last request. *URL:* /cron/twitter
+ instagram - _open_ Gets new instagram entries and stores in the DB. 

####/Entries.php - This file handles the majority of the admin UI.
+ index - _requires authentication_ Shows new entries pulled from twitter and instagram. *URL:* /entries
+ approved - _requires authentication_ Shows approved entries. Allows user to sort via drag and drop. Automatically saves order via AJAX call. *URL:* /entries/approved
+ rejected - _requires authentication_ Show rejected entries. All pages allow for changing approval status of entry. *URL:* /entries/rejected
+ addnew - _requires authentication_ Form for adding new entries manually. In place for facebook, but could be used for the other networks as well. *URL:* /entries/addnew
+ csvexport - _requires authentication_ Outputs all entries to CSV and forces a download. *URL:* /entries/csvexport
 
####/Front.php - Handles public front-end requests. Allows for language choice.
+ index - _open_ Shows front page. Defaults to English unless user has been to site before and chosen French. *URL:* /
+ fr - _open_ Sets language to French. Sends user back to index to show French page. *URL:* /fr or /front/fr
+ en - _open_ Sets language to English. Sends user back to index to show English page. *URL:* /en or /front/en
 
####/User.php - Handles authentication.
+ index - _open_ Shows login screen. Processes login after posting username and password. *URL:* /user
+ logout - _technically open_ logs out user. Forwards to login screen. *URL:* /user/logout
 
###Libraries
+ Aauth.php - handles user management and authentication. 
+ Instagram.php - handles contacting Instagram's API. Accesses /config/instagram.php for credentials.
+ Twitter.php - handles contacting Twitter's API. Accesses /config/twitter.php for credentials.
 
###Models & Database Structure
+ Entry.php - handles DB access to entry data. Accesses /config/contest.php to get entry table. Requires import of rogdis.sql in this repository.

##Contact
Matt Graham is responsible for this monstrosity. If it gives you any grief, contact him.
You can contact him via [email](mailto:matthew@mattgraham.ca), [Twitter](https://twitter.com/themattyg), [Facebook](https://www.facebook.com/themattyg) or [LinkedIn](http://mkg.io/linkedin)
This web application is largely open source software. 