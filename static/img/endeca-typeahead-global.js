/* 03/02/2015 07:53 AM   ==   v0.0.0.6.610 */

//_endecaAPI.support.url.typeahead = window.location.href.search('rogers.com') >= 0?window.location.protocol + '//' + window.location.host + '/web/': 'http://www.dev19.rogers.com/web/';
 typeaheadUrl = window.location.href.search('rogers.com') >= 0?window.location.protocol + '//' + window.location.host + '/web/': 'http://www.dev25.rogers.com/web/';

	

	// _endecaAPI.support.url.search = window.location.href.search('rogers.com') >= 0 
	// 				 ? window.location.protocol + '//' + window.location.host + '/web/content/search'
	// 				: window.location.protocol + '//' + window.location.host + '/search.result.en.shtml';
searchUrl = window.location.href.search('rogers.com') >= 0 
					 ? window.location.protocol + '//' + window.location.host + '/web/content/search'
					: window.location.protocol + '//' + window.location.host + '/search.result.en.shtml';


/*Anjana Changes start*/
function typeaheadSiteSearch(scope, $inputContainer) {
    //console.log(scope.toUpperCase() + ' Site Search Typeahead Executing...');
    // Select Global or Support Search Type url
    var searchValue = $inputContainer.val().trim();
    var eventCode = $inputContainer.attr('kc');

    if (searchValue != "") {
        if (eventCode == '13') {
        	//Prabhu Changes Starts
            //window.location.href = searchUrl + '?scope=' + scope + '&term=' + searchValue;
         	typeaheadSearch(scope,$inputContainer);
         	//Prabhu Changes Ends
  
        } else {
            if (searchValue.length >= 3) {
                var inp = String.fromCharCode(eventCode);

		searchValue = searchValue + '*';

                if (/[a-zA-Z0-9-_]/.test(searchValue) || eventCode == 8 ) {
                    var apiParams = 'suggest.jsp?_nfpb=true&_pageLabel=search&q='; // Global Search
                    //var url = 'http://10.16.203.41:8006/assembler/json/pages/rogers/browse?Ntk=global1&Nr=AND(Language:' + _rui.content.environment.language.toUpperCase() + ',Province:' + _rui.content.environment.province.toUpperCase() + ')&Ntt=';
                    if (scope.toLowerCase() == 'support') {
                        apiParams = 'suggestFaq.jsp?_nfpb=true&_pageLabel=search&q='; // Support Search
                        //url = 'http://10.16.203.41:8006/assembler/json/pages/rogers/supportsearch?Ntk=support1&N=292&Nr=AND(Language:' + _rui.content.environment.language.toUpperCase() + ',Province:' + _rui.content.environment.province.toUpperCase() + ')&Ntt=';
                    }

                    $.ajax({
                        type: "GET",
			cache: false,
                        url: typeaheadUrl + apiParams + searchValue,
                        dataType: "json",
                        success: function(data) {
                            //console.log('Site Search Typeahead Succeeded.');
                            //console.log(data); console.log("Site Search Typeahead Result data: ", JSON.stringify(data));
                            generateTypeaheadResultJSONforUI(data, scope, $inputContainer);
                        },
                        fail: function(jqXHR, textStatus) {
                            //console.log("Site Search Typeahead Request failed: " + textStatus);
                        }
                    });
                }
            }
        }

    }
}

//Prabhu Changes Starts
// This typeaheadSearch function will be used only clicking on Search Icon. Also it will be called from typeaheadSiteSearch function
function typeaheadSearch(scope, $inputContainer) {
    var searchValue = $inputContainer.val().trim();
    if (searchValue != "") {
	    if (scope != "support") {
	        var endecaAssemblerUrl = window.location.href.search('rogers.com') >= 0 
	        						? window.location.protocol + '//' + window.location.host + '/web/endeca/assembler/json' 
	        						: 'http://www.dev25.rogers.com/web/endeca/assembler/json';
				 $.ajax({
                        type: "POST",
						cache: true,
                         url: endecaAssemblerUrl,
                        dataType: "json",
						data: ({
	                    'EndecaAssemblerUrl': encodeURI('/browse' + '?Ntt=' + searchValue)
						}),
                        success: function(data) {
                            if (data.errorcode) {} else {
	                    if (data["endeca:redirect"] && scope != "support") {
	                        //window.location.href = data["endeca:redirect"].link.url;
	                        $(location).attr('href', data["endeca:redirect"].link.url);
	                    } else {
	                        window.location.href = searchUrl + '?scope=' + scope + '&term=' + searchValue;
	                    }
	                }
                        },
                        fail: function(jqXHR, textStatus) {
                            //console.log("Site Search Typeahead Request failed: " + textStatus);
                        }
                    });

					
									
									
	      /*  $.ajax({
	                type: "POST",
	                dataType: "json",
	                cache: true,
	                url: endecaAssemblerUrl,
	                data: ({
	                    'EndecaAssemblerUrl': encodeURI('/browse' + '?Ntt=' + searchValue)
	                })
	            })
	            .success(function(data) {
	                if (data.errorcode) {} else {
	                    if (data["endeca:redirect"] && scope != "support") {
	                        //window.location.href = data["endeca:redirect"].link.url;
	                        $(location).attr('href', data["endeca:redirect"].link.url);
	                    } else {
	                        window.location.href = searchUrl + '?scope=' + scope + '&term=' + searchValue;
	                    }
	                }
	            })
	            .fail(function(jqXHR, textStatus) {});*/
				
				
	    } else {
	        window.location.href = searchUrl + '?scope=' + scope + '&term=' + searchValue;
	    }
	}
}
//Prabhu Changes Ends

// Generates jsonTypeaheadResult Object for UI
function generateTypeaheadResultJSONforUI(data, scope, inputContainer){
	jsonSearchTypeaheadResult = {}; // {"related":[],"quicklinks":[]};
	// Process data
	$.each(data, function () {
		// Separate results by type (ie: Related, QuickLinks)
		$.each(this, function (index) {
			var type = this.type;
			if(type == ""){
				type = "related";
			}

			// Create New Type array if hasn't already been created
			if(!jsonSearchTypeaheadResult[type]){
				jsonSearchTypeaheadResult[type] = [];
			}
		    jsonSearchTypeaheadResult[type].push({
		    	dimensionName 	: this.dimension_name,
		    	category 		: this.category,
		    	image 			: this.image,
		    	label 			: this.label,
		    	icon 			: this.icon,
		    	url 			: this.url});
		});
    });

    //console.log("jsonSearchTypeaheadResult: ", jsonSearchTypeaheadResult); console.log("jsonSearchTypeaheadResult: ", JSON.stringify(jsonSearchTypeaheadResult));
   	displaySearchTypeaheadUI(jsonSearchTypeaheadResult, scope, inputContainer); // Have CMS display Search Typeahead Results
}

// UI Display for Search Typeahead
function displaySearchTypeaheadUI(data, scope, inputContainer){
	// Related html generation
	var path 		= window.location.protocol + '//' + window.location.host;
	var htmlRelated = '';
	if(data.related){
	
		if($("meta[name='dc.language']").attr("content").toUpperCase() == 'EN'){
			htmlRelated 	= '<div class="search-content-related"><h5>Related Searches</h5>';
		}else{
			htmlRelated 	= '<div class="search-content-related"><h5>Recherches connexes</h5>';
		}
		htmlRelatedItem = '<a href="[search page url]?scope=[scope]&term=[label]" onmousedown="javascript: window.location.href=\'[search page url]?scope=[scope]&term=[label]\';">[label]</a>';
		
		$.each(data.related, function() {
			htmlRelated += htmlRelatedItem
							.replace(/\[search page url]/g, searchUrl)
							.replace(/\[scope]/g, scope.toLowerCase())
							.replace(/\[label]/g, this.label);
			//htmlRelated += '<a href="' + _endecaAPI.support.url.search + '?scope=' + scope.toLowerCase() + '&term=' + this.label + '" onmousedown="javascript: window.location.href=\'' +_endecaAPI.support.url.search + '?scope=' + scope.toLowerCase() + '&term=' + decodeURI(this.label) + '\';">' + this.label + '</a>';
	    });
	    htmlRelated += '</div>';
	}

	// Quicklinks html generation
	var htmlQuicklinks = ''; 
	if(data['Quick Links']){
	
		if($("meta[name='dc.language']").attr("content").toUpperCase() == 'EN'){
			htmlQuicklinks = '<div class="search-content-quicklinks"><h5>Quick Links</h5>';
		}else{
			htmlQuicklinks = '<div class="search-content-quicklinks"><h5>Liens rapides</h5>';
		}
		
		$.each(data['Quick Links'], function() {
			var lUrl,iconName = '';
			if(this.url.search("http") == -1)
				lUrl = "http://" + this.url;
			else
				lUrl = this.url;

			if(this.icon == undefined || this.icon == "")
				iconName = "wireless-devices";
			else
				iconName = this.icon;

			if(this.image != ""){
				htmlQuicklinks += '<a href="' + lUrl + '" onmousedown="javascript: window.location.href=\'' + lUrl + '\';"><img src="' + path + this.image + '" /><div class="hdrimg">' + this.label + '</div></a>';
			}
			else
			{
				/*htmlQuicklinks += '<a href="' + lUrl + '" onmousedown="javascript: window.location.href=\'' + lUrl +  '\';" style="padding-left: 10px;text-decoration: none;">';
				htmlQuicklinks += '<div class="rui-icon-' + iconName +'" style="font-size: 40px;vertical-align: middle;color: grey;"></div>';
				htmlQuicklinks += '<span style="margin-left: 14px;font-size: 16px;position: static">' + this.label + '</span></a>';
				*/
				htmlQuicklinks += '<a href="' + lUrl + '" class="vectr" onmousedown="javascript: window.location.href=\'' + lUrl +  '\';">';
				htmlQuicklinks += '<div class="rui-icon-' + iconName +'"></div>';
				htmlQuicklinks += '<span>' + this.label + '</span></a>';
			}
	    });
	    htmlQuicklinks += '</div>';
	}
	
	//get current typeahead modal id
	var typeaheadModalID = inputContainer.attr('id') + '-typeahead';
	
	// if(scope == 'support' && !isResponsivePage){
	// 	//console.log("inside support");
	// 	if(htmlRelated == "" && htmlQuicklinks == ""){
	// 		$('#rui-typeahead2').hide();
	// 	}
	// 	else{
	// 		$('#rui-typeahead2').show();
	// 		$('#rui-typeahead2').html(htmlRelated + htmlQuicklinks);
	// 	}
			
	// }else{
	// 	//console.log("inside global");
	// 	if(htmlRelated == "" && htmlQuicklinks == ""){
	// 		$('#'+typeaheadModalID).hide();
	// 	}
	// 	else{
	// 		$('#'+typeaheadModalID).show();
	// 		$('#'+typeaheadModalID).html(htmlRelated + htmlQuicklinks);
	// 	}
	// }
	$('#'+typeaheadModalID).html(htmlRelated + htmlQuicklinks);
	//console.log(htmlRelated + htmlQuicklinks);
}