/* global $ */
$(function() {
    $('.btn_approve').on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        sendAjax("/api/entries/approve/"+id, $(this).closest('.theentry'));
    });
    
    $('.btn_reject').on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        sendAjax("/api/entries/reject/"+id, $(this).closest('.theentry'));
    });
    
    var sendAjax = function(url, jqobj) {
        $.post(url, {}, function(data) {
            if (parseInt(data.meta.code) == 200) {
                jqobj.fadeOut(200, function() {
                    jqobj.remove();
                });
            }
        }, 'json');
    };
    
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
    $( "#sortable" ).on( "sortupdate", function( event, ui ) {
        var list = [];
        $(this).children('li').each( function (index) {
            list.push({orderby: index, id: $(this).attr('data-id')});
        });
        $.post('/api/entries/usersort', {sortlist: list}, function(data) {
            console.log(data);
        }, 'json');
    } );
    
    $( '.theentry' ).tooltip()
});